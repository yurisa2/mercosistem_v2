<?php
Class mercomagento_products extends mercosistem_product
{
  public function __construct()
  {
    parent::__construct();
    global $magento_soap_user;
    global $magento_soap_password;

    $this->magento_obj = magento_obj();
    $this->magento_session = magento_session();
  }

  public function update_products()
  {
    $product_list = $this->magento_obj->catalogProductList($this->magento_session);
    if(!$product_list) return false;
    // var_dump($product_list); //DEBUG
    $product_list_sku = [];
    foreach ($product_list as $key => $value) {
      $product_list_sku[] = $product_list[$key]->sku;
    }
    // var_dump($product_list_sku);   //DEBUG
    if(!file_exists('include/files/magento_last_product_updated.json')) file_put_contents('include/files/magento_last_product_updated.json',json_encode(0));
    $last_product_updated = json_decode(file_get_contents('include/files/magento_last_product_updated.json'));

    $index = array_search($last_product_updated,$product_list_sku);

    if($index+1 >= count($product_list_sku)) $product_updated = $product_list_sku[0];
    else $product_updated = $product_list_sku[$index+1];

    if(!$last_product_updated) $product_updated = $product_list_sku['0'];
    echo "SKU a ser atualizado: ";
    var_dump($product_updated);
    $obj_produts = $this->list_product($product_updated)->sBody->ProdutoPorFiltroDisponiveisResponse->ProdutoPorFiltroDisponiveisResult->aProduto;
    if(is_null($obj_produts)) {
      file_put_contents('include/files/product_error.json',json_encode($product_updated));
      return false;
    } else {
      if(strpos($obj_produts->a_desc_reduz, "-")) {
        $prod_magento['name'] = substr($obj_produts->a_desc_reduz,0,strpos($obj_produts->a_desc_reduz, "-"));
      } else $prod_magento['name'] = $obj_produts->a_desc_reduz;
      $prod_magento['description'] = nl2br($obj_produts->a_aplicacao);
      $prod_magento['short_description'] = nl2br($obj_produts->a_obs);
      $prod_magento['weight'] = $obj_produts->a_peso_bruto;
      $price_list = $this->get_product_prices($product_updated);
      // echo "price_list";     DEBUG
      // var_dump($price_list); DEBUG

      $prod_magento['price'] = $price_list['preco2'];

      if($product_updated == "EP-51-60011") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35101");
      elseif ($product_updated == "EP-51-60021") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35011");
      elseif ($product_updated == "EP-51-60031") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35021");
      elseif ($product_updated == "EP-51-60041") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35041");
      elseif ($product_updated == "EP-51-60056") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35812");
      elseif ($product_updated == "EP-51-60061") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35051");
      elseif ($product_updated == "EP-51-60086") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35754");
      elseif ($product_updated == "EP-51-60116") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35755");
      elseif ($product_updated == "EP-51-60096") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35771");
      elseif ($product_updated == "EP-51-60106") $prod_magento['qty_in_stock'] = $this->get_product_qty("EP-51-35772");

      else $prod_magento['qty_in_stock'] = $this->get_product_qty($obj_produts->a_cod_fab);

      $tier_price_revenda = array(
        'customer_group_id' => 4,
        'website' => 'all',
        'qty' => 1,
        'price' => $price_list['preco1']
      );

      $tier_price_logado = array(
        'customer_group_id' => 1,
        'website' => 'all',
        'qty' => 1,
        'price' => $price_list['preco3']
      );

      $tier_price_notlog = array(
        'customer_group_id' => 0,
        'website' => 'all',
        'qty' => 1,
        'price' => $price_list['preco2']
      );

      $tier_price_promocao = array(
        'customer_group_id' => 5,
        'website' => 'all',
        'qty' => 1,
        'price' => $price_list['preco4']
      );

      $prod_magento['tier_price'] = array(
        $tier_price_revenda,
        $tier_price_logado,
        $tier_price_notlog,
        $tier_price_promocao,
      );

      if(empty($prod_magento)) {
        return false;
      }

    }
    // var_dump($prod_magento);
    $update = magento_catalogProductUpdate_and_stock($product_updated,$prod_magento);

    if(!$update) {
      file_put_contents('include/files/product_error.json',json_encode($product_updated));
      return false;
    }
    file_put_contents('include/files/magento_last_product_updated.json',json_encode($product_updated));
    return $update;
  }

  public function get_product_prices($sku){
    $cod = $this->get_product_code($sku);

    $soap_list = $this->get_prices();

    foreach ($soap_list as $key => $value) {
      $descricao_list = $soap_list[$key]->_descricao;

      if($descricao_list == "SITE") {
        $list = $soap_list[$key]->_ListaProdutoList->ListaProduto;
        break;
      }
    }

    foreach ($list as $key => $value) {
      $product_id = (int)$list[$key]->_cod_prod;
      // $id = (int)$id;

      if($product_id == $cod) {
        $return = [];
        if(!empty($list[$key]->_vlr_1)) {
          $return['preco1'] = (float)$list[$key]->_vlr_1;
        } else {
          $return['preco1'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_2)) {
          $return['preco2'] = (float)$list[$key]->_vlr_2;
        } else {
          $return['preco2'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_3)) {
          $return['preco3'] = (float)$list[$key]->_vlr_3;
        } else {
          $return['preco3'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_4)) {
          $return['preco4'] = (float)$list[$key]->_vlr_4;
        } else {
          $return['preco4'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_5)) {
          $return['preco5'] = (float)$list[$key]->_vlr_5;
        } else {
          $return['preco5'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_base)) {
          $return['valor_base'] = (float)$list[$key]->_vlr_base;
        } else {
            $return['valor_base'] = (float)0;
        }

        return $return;
        break;
      }
    }
  }
}

?>
