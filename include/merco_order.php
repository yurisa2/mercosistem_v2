<?php
Class merco_order extends magento_order
{
  public function __construct()
  {
    parent::__construct();
    $this->mercosistem_order = new mercosistem_order;
    $this->mercosistem_customer = new mercosistem_customer;
  }

  public function MercoMagento_create_order($order_id)
  {
    // var_dump($this->mercosistem_order->get_order($order_id)); //DEBUG

        //get information about magento order
        $order_info = $this->get_order_info($order_id);
        
        foreach ($order_info->status_history as $key => $value) {
          if(isset($value->comment) && strpos($value->comment,"->")) {
            $order_comment = explode("->",$value->comment);
          }
        }

        if(isset($order_comment)) {
          $comment = [];
          foreach ($order_comment as $key => $value) {
            $comment[] = substr($value, strpos($value,":")+1);
          }
        }
        // echo "Info order:<br> ";   //DEBUG
        // var_dump($comment);     //DEBUG
        // exit("Commwnt Araay");
        if(isset($order_info->customer_id)) {
          $customer_address = $this->get_customer_address_list($order_info->customer_id)[0];
          $customer_info['name'] = $customer_address->firstname." ".$customer_address->lastname;
          $customer_info['city'] = $customer_address->city;
          $customer_info['region'] = $customer_address->region;
          $customer_info['postcode'] = preg_replace('/\D/', '',$customer_address->postcode);
          $customer_info['street'] = $customer_address->street;
          $customer_info['phone'] = preg_replace('/\D/', '',$customer_address->telephone);

          $customer = $this->get_customer_info($order_info->customer_id);
          $customer_info['document'] = preg_replace('/\D/', '',$customer->taxvat);
          $customer_info['email'] = $customer->email;

          if(strlen($customer_info['region']) > 2) $customer_info['region'] = $this->mercosistem_customer->changestate($customer_info['region']);
          $index_street = strpos($customer_info['street'], ",");
          $index_number = strpos($customer_info['street'], "-");
          $customer_info['new_street'] = substr($customer_info['street'], 0, $index_street);
          $index_n = $index_number - strlen($customer_info['street']);
          $customer_info['number'] = substr($customer_info['street'], $index_street+1, $index_n);
          $customer_info['neighborhood'] = substr($customer_info['street'], $index_number+1);
          $customer_info['date_of_birth'] = $comment[10];
          /*HOMOLOGAÇÃO*/$customer_info['cfop'] = '5102';
        } else {
          $customer_info['name'] = $order_info->customer_firstname.' '.$order_info->customer_lastname;
          $customer_info['email'] = $order_info->customer_email;
          $customer_info['document'] = '';
          $customer_info['city'] = $order_info->shipping_address->city;
          $customer_info['region'] = $order_info->shipping_address->region;
          $customer_info['postcode'] = $order_info->shipping_address->postcode;
          $customer_info['phone'] = $order_info->shipping_address->telephone;
          $customer_info['street'] = trim(preg_replace("/\([^)]+\)/","",$order_info->shipping_address->street));
          $customer_info['new_street'] = substr($customer_info['street'], 0, strpos($customer_info['street'],','));
          $customer_info['number'] = (int)substr($customer_info['street'],strpos($customer_info['street'],',')+1);
          $customer_info['neighborhood'] = trim(substr($customer_info['street'],strpos($customer_info['street'],(string)$customer_info['number'])+strlen($customer_info['number'])));
          $customer_info['date_of_birth'] = '' ;
          // /*HOMOLOGAÇÃO*/$customer_info['cfop'] = '5102';
          if(strlen($customer_info['region']) > 2) $customer_info['region'] = $this->mercosistem_customer->changestate($customer_info['region']);
          foreach ($order_info->items as $key => $value) {
            $original_price[] = $value->original_price;
            $price[] = $value->price;
          }
          $comment = array('Pedido feito pelo site','Sem Referência','Sem Referência','Credit cart',$original_price,$price,$order_info->subtotal,$order_info->discount_amount,
          $order_info->shipping_amount,$order_info->grand_total,'','');
        }
  // var_dump($customer_info);
  // exit("Parametro para insert_customer()");
        $customer_id = $this->mercosistem_customer->insert_customer($customer_info);
        if(!$customer_id) {
          $nome_funcao = "Não foi possível identificar o id do cliente";
          $saida = "Possível problema com endereço ou nome do cliente";
          $titulo = "Erro ao cadastrar cliente no Mercosistem";
          //estancia a classe com os parametros
          $error_handling = new error_handling($titulo, $nome_funcao, $saida, "erro");
          //estancia a função para criar a mensagem de corpo
          $error_handling->send_error_email();
          //estancia a função para executar as funções email()-db()-files() previamente
          //por padrão, as propriedades error_db e error_files estão true
          $error_handling->execute();
          echo "<br>Erro ao cadastrar o cliente ";
          return false;
        }

        $order_data = array(
          'customer_id' => $customer_id,
          'order_id' => $order_id,
          'shipping_amount' => $comment[8],
          'total' => $comment[9],
          'subtotal' => $comment[6],
          'shipping_description' => $order_info->shipping_description,
          'created_at' => $order_info->created_at,
          // 'shipping_firstname' => "Teste Cadastro Cliente",
          'shipping_firstname' => $order_info->shipping_address->firstname." ".$order_info->shipping_address->lastname,
          'shipping_street' => $order_info->shipping_address->street,
          'shipping_city' => $order_info->shipping_address->city,
          'shipping_region' => $order_info->shipping_address->region,
          'shipping_postcode' => $order_info->shipping_address->postcode,
          'shipping_telephone' => $order_info->shipping_address->telephone,
          // 'billing_firstname' => "Teste Cadastro Cliente",
          'billing_firstname' => $order_info->billing_address->firstname." ".$order_info->billing_address->lastname,
          'billing_street' => $order_info->billing_address->street,
          'billing_city' => $order_info->billing_address->city,
          'billing_region' => $order_info->billing_address->region,
          'billing_postcode' => $order_info->billing_address->postcode,
          'billing_telephone' => $order_info->billing_address->telephone);

        if(gettype($comment[5]) == 'array') {
          foreach ($order_info->items as $key => $value) {
            $items['items'][] = array('CodigoProd' => $value->sku,
            'Qtde' => (int)$value->qty_ordered,
            'ValUnit' => (float)$comment[5][$key],
            'comment' => $comment);
          }
        } else {
          foreach ($order_info->items as $key => $value) {
            $items['items'][] = array('CodigoProd' => $value->sku,
            'Qtde' => (int)$value->qty_ordered,
            'ValUnit' => (float)$comment[5],
            'comment' => $comment);
          }
        }
        $order_data = array_merge($customer_info,$order_data);
        $order_data = array_merge($order_data,$items);

        $result = $this->mercosistem_order->create_order($order_data);

        // var_dump($result);        //DEBUG
        // exit("retorno Pedido");     //DEBUG
        if(!$result) {
          $nome_funcao = "Função create_order(): $order_id";
          $saida = "erro";
          $titulo = "Erro ao criar pedido no Mercosistem";
          //estancia a classe com os parametros
          $error_handling = new error_handling($titulo, $nome_funcao, $saida, "erro");
          //estancia a função para criar a mensagem de corpo
          $error_handling->send_error_email();
          //estancia a função para executar as funções email()-db()-files() previamente
          //por padrão, as propriedades error_db e error_files estão true
          $error_handling->execute();
          echo "<br>Erro ao criar o pedido ";
          return false;
        } else {
        $merco_id = $this->mercosistem_order->get_order($order_id);
        $return =  $this->add_order_comment($order_id,"Id do Mercosistem: ".$merco_id->aCodigo);

        $corpo1 = "Pedido: ".$comment[0].
        "<br>Pedido do Magento: ". $result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aNumPedido.
        "<br>Pedido do Mercosistem: ".$merco_id->aCodigo;
        $corpo2 = "Cliente: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestRazao.
        "<br>E-mail: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestEmail.
        "<br>Data: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDataEmissao.
        "<br>Cep: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestCepEntr.
        "<br>Cidade: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestCidadeEntr.
        "<br>Estado: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestEstadoEntr.
        "<br>Endereço: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestEnderecoEntr.
        "<br>Subtotal da Compra: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aValorProdutos.
        "<br>Valor do Frete: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aValorFrete.
        "<br>Valor Total da Compra: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aValorPedido.
        // "<br>PRODUTO: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aItem->aItemPedido->aCodigoProd.
        "<br>Qtde: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aItem->aItemPedido->aQtde;

        if(file_exists(str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/pedidos/'.$comment[0].'.json')) {
        $mensagem_b2w = file_get_contents(str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/pedidos/'.$comment[0].'.json');
        $corpo1 .= $mensagem_b2w;
        }
        //estancia a classe com os parametros
        $log = new log("Nova Compra Cadastrada no Mercosistem", $corpo1, $corpo2, "nova compra");
        $log->dir_files = "log_files/log.json";
        $log->log_email = true;
        $log->log_etiqueta = str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/pedidos/'.$comment[0].'.pdf';
        $log->log_db = true;
        $log->log_files = true;
        $log->mensagem_email = "Nova compra que entrou no mercosistem";
        $log->email_novacompra = true;
        //estancia a função para criar a mensagem de corpo
        $log->send_log_email();
        //estancia a função para executar as funções email()-db()-files() previamente
        //por padrão, as propriedades error_db e error_files estão true
        $log->execute();
        var_dump($result->sBody->InserirPedidoResponse->InserirPedidoResult);
        return "Pedido Magento $order_id <b>OK</b>";
    }
  }
}

 ?>
