<?php
Class merco_products extends magento_product
{
  public function __construct()
  {
    parent::__construct();
    $this->product_id = '';
    $this->product_name = '';
    $this->product_price_list = '';
    $this->product_weight = '';
    $this->product_width = '';
    $this->product_length = '';
    $this->product_height = '';
    $this->product_description = '';
    $this->product_short_description = '';

    $this->mercosistem = new mercosistem_product;
  }

  public function update_products()
  {
    echo "<h2>Produto $this->product_id </h2>";
    if(TITLE) {
      if(!empty($this->product_name)) {
        if(strpos($this->product_name, "-")) {
          $prod_magento['name'] = substr($this->product_name,0,strpos($this->product_name, "-"));
        } else $prod_magento['name'] = $this->product_name;
        $url_key = $this->product_id;
        $url_key .= "-";
        $url_key .= str_replace(' ', '-', $prod_magento['name']);
        $url_key = removeAccents($url_key);
        $prod_magento['url_key'] = $url_key;
      }
    } else echo "<h3>TITLE Desligado</h3>";
    if(DESCRIPTION) {
      if(!empty($this->product_description)) {
        $prod_magento['description'] = $this->product_description;
        $prod_magento['short_description'] = $this->product_short_description;
      }
    } else echo "<h3>DESCRIPTION Desligado</h3>";

    if(!file_exists('atributosAusentes.json')) file_put_contents("atributosAusentes.json",'');
    $WithoutAttributes = (array)json_decode(file_get_contents("atributosAusentes.json"));

    if(!empty($this->product_weight)) {
      $prod_magento['weight'] = $this->product_weight;
    } else {
      $WithoutAttributes[$this->product_id]->peso = $this->product_weight;
      file_put_contents("atributosAusentes.json",json_encode($WithoutAttributes));
    }

    if(!empty($this->product_width)) {
      $prod_magento['additional_attributes']['single_data'][] = array(
        'key'=> 'width',
        'value' => $this->product_width/1000
      );
    } else {
      $WithoutAttributes[$this->product_id]->largura = $this->product_width;
      file_put_contents("atributosAusentes.json",json_encode($WithoutAttributes));
    }

    if(!empty($this->product_length)) {
      $prod_magento['additional_attributes']['single_data'][] = array(
        'key'=> 'length',
        'value' => $this->product_length/1000
      );
    } else {
      $WithoutAttributes[$this->product_id]->comprimento = $this->product_length;
      file_put_contents("atributosAusentes.json",json_encode($WithoutAttributes));
    }

    if(!empty($this->product_height)) {
      $prod_magento['additional_attributes']['single_data'][] = array(
        'key'=> 'height',
        'value' => $this->product_height/1000
      );
    } else {
      $WithoutAttributes[$this->product_id]->altura = $this->product_height;
      file_put_contents("atributosAusentes.json",json_encode($WithoutAttributes));
    }

    if(PRICE) {
      if(!empty($this->product_price_list)) {
        $prod_magento['price'] = $this->product_price_list['preco2'];
      }
    }

    if(!empty($prod_magento)) {
      $update_product = $this->update_product($this->product_id,$prod_magento);
      if(!$update_product) {
        $error = new error_handling("MERCOSISTEM: Erro ao atualizar produto", "Erro ao tentar atualizar informações básicas do produto $this->product_id", "Erro ao atualizar title, price, description e short description", "Erro produto");
        $error->send_error_email();
        $error->execute();
        echo "Erro ao atualizar produto $this->product_id";
      } else echo "INFORMAÇÕES BÁSICAS: <b>OK</b><br>";
    } else throw new Exception('Informações sobre o produto estão vazias');

    if(STOCK) {
      if($this->product_id == "EP-51-60011") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35101");
      elseif ($this->product_id == "EP-51-60021") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35011");
      elseif ($this->product_id == "EP-51-60031") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35021");
      elseif ($this->product_id == "EP-51-60041") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35041");
      elseif ($this->product_id == "EP-51-60056") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35812");
      elseif ($this->product_id == "EP-51-60061") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35051");
      elseif ($this->product_id == "EP-51-60086") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35754");
      elseif ($this->product_id == "EP-51-60116") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35755");
      elseif ($this->product_id == "EP-51-60096") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35771");
      elseif ($this->product_id == "EP-51-60106") $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty("EP-51-35772");

      else $prod_magento['qty_in_stock'] = $this->mercosistem->get_product_qty($this->product_id);

      $update_productStock = $this->update_product_stock($this->product_id,$prod_magento['qty_in_stock']);
      if($update_productStock == 0) {
        $error = new error_handling("MERCOSISTEM: Produto desativado", "Produto $this->product_id foi desativado", "Estoque do produto: ".$prod_magento['qty_in_stock'], "Produto desativado");
        $error->send_error_email();
        $error->execute();
        echo "Produto $this->product_id desativado";
      }
      echo "ESTOQUE: ".$prod_magento['qty_in_stock']." <b>OK</b><br>";
      // var_dump($update_productStock);   //DEBUG
    } else echo "<h3>STOCK Desligado</h3>";

    if(PRICE) {
      $tier_price_revenda = array(
        'customer_group_id' => 4,
        'website' => 'all',
        'qty' => 1,
        'price' => $this->product_price_list['preco1']
      );

      $tier_price_notlog = array(
        'customer_group_id' => 0,
        'website' => 'all',
        'qty' => 1,
        'price' => $this->product_price_list['preco2']
      );

      $tier_price_logado = array(
        'customer_group_id' => 1,
        'website' => 'all',
        'qty' => 1,
        'price' => $this->product_price_list['preco3']
      );

      $tier_price_promocao = array(
        'customer_group_id' => 5,
        'website' => 'all',
        'qty' => 1,
        'price' => $this->product_price_list['preco4']
      );

      $prod_magento['tier_price'] = array(
        $tier_price_revenda,
        $tier_price_logado,
        $tier_price_notlog,
        $tier_price_promocao,
      );

      if(empty($prod_magento)) return "Informações do produto $this->product_id vazias";

      (bool)$update_productPrice = $this->update_product_price($this->product_id, $prod_magento['tier_price']);
      if(!$update_productPrice) {
        $error = new error_handling("MERCOSISTEM: Erro ao atualizar precos produto", "Erro ao tentar atualizar os preços do produto $this->product_id", " ", "Erro produto");
        $error->send_error_email();
        $error->execute();
        echo "Erro ao atualizar preços do produto $this->product_id";
      } else {
        echo "PREÇO: ".$this->product_price_list['preco2']." <b>OK</b><br>";
        // var_dump($update_productPrice);   //DEBUG
      }
    } else echo "<h3>PRICE Desligado</h3>";
  }

  public function get_product_prices($sku,$price_list){
    // $sku = $this->get_product_code($sku);

    // $price_list = $this->get_prices();

    foreach ($price_list as $key => $value) {
      $descricao_list = $price_list[$key]->_descricao;
      if($descricao_list == "SITE") {
        $list = $price_list[$key]->_ListaProdutoList->ListaProduto;
        break;
      }
    }
    foreach ($list as $key => $value) {
      $product_id = (int)$list[$key]->_cod_prod;
      // $id = (int)$id;
      if($product_id == $sku) {
        $return = [];
        if(!empty($list[$key]->_vlr_1)) {
          $return['preco1'] = (float)$list[$key]->_vlr_1;
        } else {
          $return['preco1'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_2)) {
          $return['preco2'] = (float)$list[$key]->_vlr_2;
        } else {
          $return['preco2'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_3)) {
          $return['preco3'] = (float)$list[$key]->_vlr_3;
        } else {
          $return['preco3'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_4)) {
          $return['preco4'] = (float)$list[$key]->_vlr_4;
        } else {
          $return['preco4'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_5)) {
          $return['preco5'] = (float)$list[$key]->_vlr_5;
        } else {
          $return['preco5'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_base)) {
          $return['valor_base'] = (float)$list[$key]->_vlr_base;
        } else {
          $return['valor_base'] = (float)0;
        }
        return $return;
        break;
      }
      $return['preco1'] = (float)0;
      $return['preco2'] = (float)0;
      $return['preco3'] = (float)0;
      $return['preco4'] = (float)0;
      $return['preco5'] = (float)0;
      $return['valor_base'] = (float)0;

      return $return;
    }
  }
}

?>
