<?php
Class mercomagento_orders
{
  public function __construct()
  {
    global $magento_soap_user;
    global $magento_soap_password;

    $this->magento_obj = magento_obj();
    $this->magento_session = magento_session();

    $this->class_orders = new mercosistem_orders;
    $this->class_customer = new mercosistem_customer;
  }

  public function create_order()
  {
    $order_list = $this->magento_obj->salesOrderList($this->magento_session);
    if(!$order_list) return false;
    $array_orders_id= [];

    foreach ($order_list as $key => $value) {
      $array_orders_id[] = $order_list[$key]->increment_id;
    }
    // echo "lista de pedidos: ";    // DEBUG
    // var_dump($array_orders_id);  //DEBUG
    // exit();
    if(!file_exists('include/files/magento_last_order_updated.json')) file_put_contents('include/files/magento_last_order_updated.json',json_encode(0));
    $last_order = json_decode(file_get_contents('include/files/magento_last_order_updated.json'));

    echo "Ultimo pedido cadastrado: $last_order<br>";  // DEBUG

    if(!$last_order) return "Não foi possível ler json com o ultimo pedido";

    $index = array_search($last_order,$array_orders_id);
    if($index+1 == count($array_orders_id)) return "Não há novos pedidos";
    else $order_updated = $array_orders_id[$index+1];
    echo "<b>Pedido: </b>";
    var_dump($order_updated);
    echo "<b>Pedido existente? </b>";

    var_dump($this->class_orders->get_order($order_updated));
    if(!$this->class_orders->get_order($order_updated)) {
      $order_info = $this->magento_obj->salesOrderInfo($this->magento_session, $order_updated);

      foreach ($order_info->status_history as $key => $value) {
        if(isset($value->comment) && strpos($value->comment,"->")) {
          $order_comment = explode("->",$value->comment);
        }
      }

      if(isset($order_comment)) {
        $comment = [];
        foreach ($order_comment as $key => $value) {
          $comment[] = substr($value, strpos($value,":")+1);
        }
      }
      // echo "Info order:<br> ";   //DEBUG
      // var_dump($comment);     //DEBUG
      // exit("Commwnt Araay");
      if(isset($order_info->customer_id)) {
      $customer_info = magento_customerCustomerList($order_info->customer_id);
      if(strlen($customer_info['region']) > 2) $customer_info['region'] = $this->class_customer->changestate($customer_info['region']);
      $index_street = strpos($customer_info['street'], ",");
      $index_number = strpos($customer_info['street'], "-");
      $customer_info['new_street'] = substr($customer_info['street'], 0, $index_street);
      $index_n = $index_number - strlen($customer_info['street']);
      $customer_info['number'] = substr($customer_info['street'], $index_street+1, $index_n);
      $customer_info['neighborhood'] = substr($customer_info['street'], $index_number+1);
      $customer_info['date_of_birth'] = $comment[10];
    } else {
      $customer_info['name'] = $order_info->customer_firstname.' '.$order_info->customer_lastname;
      $customer_info['email'] = $order_info->customer_email;
      $customer_info['document'] = '';
      $customer_info['city'] = $order_info->shipping_address->city;
      $customer_info['region'] = $order_info->shipping_address->region;
      $customer_info['postcode'] = $order_info->shipping_address->postcode;
      $customer_info['phone'] = $order_info->shipping_address->telephone;
      $customer_info['street'] = trim(preg_replace("/\([^)]+\)/","",$order_info->shipping_address->street));
      $customer_info['new_street'] = substr($customer_info['street'], 0, strpos($customer_info['street'],','));
      $customer_info['number'] = (int)substr($customer_info['street'],strpos($customer_info['street'],',')+1);
      $customer_info['neighborhood'] = trim(substr($customer_info['street'],strpos($customer_info['street'],(string)$customer_info['number'])+strlen($customer_info['number'])));
      $customer_info['date_of_birth'] = '' ;
      if(strlen($customer_info['region']) > 2) $customer_info['region'] = $this->class_customer->changestate($customer_info['region']);
      foreach ($order_info->items as $key => $value) {
        $original_price[] = $value->original_price;
        $price[] = $value->price;
      }
      $comment = array('Pedido feito pelo site','Sem Referência','Sem Referência','Credit cart',$original_price,$price,$order_info->subtotal,$order_info->discount_amount,
      $order_info->shipping_amount,$order_info->grand_total,'','');
    }
// var_dump($customer_info);
// exit("Parametro para insert_customer()");
      $customer_id = $this->class_customer->insert_customer($customer_info);
      if(!$customer_id) {
        echo "Não foi possível identificar o id do cliente";
        return false;
      }

      $order_data = array(
        'customer_id' => $customer_id,
         'order_id' => $order_updated,
         'shipping_amount' => $comment[8],
         'total' => $comment[9],
         'subtotal' => $comment[6],
         'shipping_description' => $order_info->shipping_description,
        'created_at' => $order_info->created_at,
        // 'shipping_firstname' => "Teste Cadastro Cliente",
        'shipping_firstname' => $order_info->shipping_address->firstname." ".$order_info->shipping_address->lastname,
        'shipping_street' => $order_info->shipping_address->street,
        'shipping_city' => $order_info->shipping_address->city,
        'shipping_region' => $order_info->shipping_address->region,
          'shipping_postcode' => $order_info->shipping_address->postcode,
          'shipping_telephone' => $order_info->shipping_address->telephone,
          // 'billing_firstname' => "Teste Cadastro Cliente",
          'billing_firstname' => $order_info->billing_address->firstname." ".$order_info->billing_address->lastname,
           'billing_street' => $order_info->billing_address->street,
          'billing_city' => $order_info->billing_address->city,
            'billing_region' => $order_info->billing_address->region,
            'billing_postcode' => $order_info->billing_address->postcode,
            'billing_telephone' => $order_info->billing_address->telephone);

      if(gettype($comment[5]) == 'array') {
        foreach ($order_info->items as $key => $value) {
          $items['items'][] = array('CodigoProd' => $value->sku,
          'Qtde' => (int)$value->qty_ordered,
          'ValUnit' => (float)$comment[5][$key],
          'comment' => $comment);
        }
      } else {
        foreach ($order_info->items as $key => $value) {
          $items['items'][] = array('CodigoProd' => $value->sku,
          'Qtde' => (int)$value->qty_ordered,
          'ValUnit' => (float)$comment[5],
          'comment' => $comment);
        }
      }
      $order_data = array_merge($customer_info,$order_data);
      $order_data = array_merge($order_data,$items);

      $result = $this->class_orders->create_order($order_data);
      file_put_contents('include/files/magento_last_order_updated.json',json_encode($order_updated));

      if(isset($result->sBody->sFault))
      {
        $nome_funcao = "Função create_order(): $order_updated";
        $saida = (string)$result->sBody->sFault->faultstring;
        $titulo = "Erro ao criar order no Mercosistem";
        //estancia a classe com os parametros
        $error_handling = new error_handling($titulo, $nome_funcao, $saida, "erro");
        //estancia a função para criar a mensagem de corpo
        $error_handling->send_error_email();
        //estancia a função para executar as funções email()-db()-files() previamente
        //por padrão, as propriedades error_db e error_files estão true
        $error_handling->execute();
        return false;
      }

      $merco_id = $this->class_orders->get_order($order_updated);
      $return =  $this->magento_obj->salesOrderAddComment($this->magento_session, $order_updated, 'pending', "Id do Mercosistem: ".$merco_id->aCodigo, null);

      $corpo1 = "Pedido: ".$comment[0].
      "<br>Pedido do Magento: ". $result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aNumPedido.
      "<br>Pedido do Mercosistem: ".$merco_id->aCodigo;
      $corpo2 = "CLIENTE: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestRazao.
      "<br>E-MAIL: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestEmail.
      "<br>DATA: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDataEmissao.
      "<br>CEP: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestCepEntr.
      "<br>CIDADE: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestCidadeEntr.
      "<br>ESTADO: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestEstadoEntr.
      "<br>ENDERECO: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestEnderecoEntr.
      "<br>SUBTOTAL DA COMPRA: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aValorProdutos.
      "<br>VALOR DO FRETE: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aValorFrete.
      "<br>VALOR TOTAL DA COMPRA: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aValorPedido.
      // "<br>PRODUTO: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aItem->aItemPedido->aCodigoProd.
      "<br>QTDE: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aItem->aItemPedido->aQtde;
      //estancia a classe com os parametros
      $log = new log("Nova Compra Cadastrada no Mercosistem", $corpo1, $corpo2, "nova compra");
      $log->dir_files = "log_files/log.json";
      $log->log_email = true;
      $log->log_db = true;
      $log->log_files = true;
      $log->mensagem_email = "Nova compra que entrou no mercosistem";
      $log->email_novacompra = true;
      //estancia a função para criar a mensagem de corpo
      $log->send_log_email();
      //estancia a função para executar as funções email()-db()-files() previamente
      //por padrão, as propriedades error_db e error_files estão true
      $log->execute();
      return $result->sBody->InserirPedidoResponse->InserirPedidoResult;
    } else {
      echo "Pedido já cadastrado no MercoSistem<br>";
      $result = file_put_contents('include/files/magento_last_order_updated.json',json_encode($order_updated));
      if(!$result) echo "Não foi possível gravar o código no json >magento_last_order_updated<<br>";
      return true;
    }
  }
}

 ?>
