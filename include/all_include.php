<?php
$prefix_m = 'include/apimagentophp/';
$prefix_merco = 'include/apimercosistemphp/';
$prefix_wc = 'include/apiwoocommercephp/';

require_once 'include/config.php';

require_once 'include/mail/corpo_email.php';  //corpo e-mail
require_once 'include/mail/mail.php';         //PHPMailer

require_once 'include/event_base.php';        //control class: email/file/db
require_once 'include/error_handling.php';    //error class
require_once 'include/log.php';               //log class

require_once 'include/apimagentophp/include/all_include.php';
require_once 'include/apimercosistemphp/include/all_include.php';
require_once 'include/sa2_flux/include/flux.php';

require_once 'include/apiwoocommercephp/include/all_include.php';
require_once 'include/mercowc_product.php';
require_once 'include/mercowc_order.php';


require_once 'include/mercomagento_products.php';
require_once 'include/mercomagento_orders.php';

require_once 'include/defines.php';

require_once 'include/merco_products.php';
require_once 'include/merco_order.php';

require_once 'include/PHPMailer/src/Exception.php';
require_once 'include/PHPMailer/src/PHPMailer.php';
require_once 'include/PHPMailer/src/SMTP.php';

?>
