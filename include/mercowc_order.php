<?php
Class mercowc_order extends wooCommerceOrder
{
    public $metaDataMktPlace = null;
    public $metaDataWebSite = null;

  public function __construct()
  {
    parent::__construct();
    $this->wooCommerceCustomer = new wooCommerceCustomer;
    $this->mercosistem_order = new mercosistem_order;
    $this->mercosistem_customer = new mercosistem_customer;
    $this->mercosistem_product = new mercosistem_product;

    $this->customerData = new stdClass;
    $this->orderData = new stdClass;
  }

  public function mercoWcCreateOrder($order_id)
  {

    $orderInformation = $this->wooCommerceGetOrder($order_id);

    // var_dump($orderInformation);       //DEBUG
    // exit;                                //DEBUG

    foreach ($orderInformation->meta_data as $key => $value) {
      if($value->key == "orderId") $this->metaDataMktPlace['orderId'] = $value->value;
      if($value->key == "shippingReference") $this->metaDataMktPlace['shippingReference'] = $value->value;
      if($value->key == "billingReference") $this->metaDataMktPlace['billingReference'] = $value->value;
      // if($value->key == "paymentType") $this->metaDataMktPlace['paymentType'] = $value->value;
      // if($value->key == "productOriginalPrice") $this->metaDataMktPlace['productOriginalPrice'] = $value->value;
      // if($value->key == "productSpecialPrice") $this->metaDataMktPlace['productSpecialPrice'] = $value->value;
      if($value->key == "subtotal") $this->metaDataMktPlace['subtotal'] = $value->value;
      if($value->key == "discount") $this->metaDataMktPlace['discount'] = $value->value;
      if($value->key == "totalToPay") $this->metaDataMktPlace['totalToPay'] = $value->value;
      if($value->key == "birthday") $this->metaDataMktPlace['birthday'] = $value->value;
      // if($value->key == "parcels") $this->metaDataMktPlace['parcels'] = $value->value;
      if($value->key == "commission") $this->metaDataMktPlace['commission'] = $value->value;
      if($value->key == "shipping") $this->metaDataMktPlace['shipping'] = $value->value;

      if($value->key == "Tipo de pagamento") $this->metaDataWebSite['paymentType'] = $value->value;
      if($value->key == "Parcelas") $this->metaDataWebSite['parcels'] = $value->value;
      if($value->key == "Nome do comprador") $this->metaDataWebSite['buyer'] = $value->value;
      if($value->key ==  "_shipping_neighborhood") $this->metaDataWebSite['_shipping_neighborhood'] = $value->value;
      if($value->key ==  "_shipping_number") $this->metaDataWebSite['_shipping_number'] = $value->value;
      if($value->key ==  "_billing_neighborhood") $this->metaDataWebSite['_billing_neighborhood'] = $value->value;
      if($value->key ==  "_billing_number") $this->metaDataWebSite['_billing_number'] = $value->value;
      if($value->key ==  "_billing_cpf") $this->metaDataWebSite['_billing_cpf'] = $value->value;
    }


    if(is_null($this->metaDataMktPlace)) {
      $this->metaDataWebSite['orderId'] = "Pedido feito pelo site";
      $this->metaDataWebSite['shippingReference'] = "Sem Referência";
      $this->metaDataWebSite['billingReference'] = "Sem Referência";
      $this->metaDataWebSite['discount'] = $orderInformation->discount_total;
    }

    $this->metaDataWebSite['totalToPay'] = $orderInformation->total;

    $normalizedCustomerData = $this->mercoWcNormalizeCustomerData($orderInformation);

    // $normalizedOrderData = $this->mercoWcNormalizeOrderData($orderInformation);

    // var_dump(file_exists(str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.str_replace(' ','',$this->orderId).'.json'));
    // var_dump(file_exists(str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.$this->orderId.'.pdf'));
    // exit;
    // echo "<br><br><br> Dados Pedido metaDataMktPlace";
    // var_dump($this->metaDataMktPlace);
    // echo "<br><br><br> Dados Pedido metaDataWebSite";
    // var_dump($this->metaDataWebSite);

    $this->customerId = $this->mercosistem_customer->insert_customer($normalizedCustomerData);

    // exit;
    // $this->customerId = '8137';
    if(!$this->customerId) {
      $nome_funcao = "Não foi possível identificar o id do cliente";
      $saida = "Possível problema com endereço ou nome do cliente<br>Pedido Woocommerce: $order_id";
      $titulo = "Erro ao cadastrar cliente no Mercosistem";
      //estancia a classe com os parametros
      $error_handling = new error_handling($titulo, $nome_funcao, $saida, "erro");
      //estancia a função para criar a mensagem de corpo
      $error_handling->send_error_email();
      //estancia a função para executar as funções email()-db()-files() previamente
      //por padrão, as propriedades error_db e error_files estão true
      $error_handling->execute();
      echo "<br>Erro ao cadastrar o cliente ";
      return false;
    }

    $normalizedOrderData = $this->mercoWcNormalizeOrderData($orderInformation);


    // order data
    $result = $this->mercosistem_order->create_order($normalizedOrderData);

    // var_dump($result);        //DEBUG
    // exit("retorno Pedido");     //DEBUG
    if(!$result) {
      $nome_funcao = "Função create_order(): $order_id";
      $saida = "erro";
      $titulo = "Erro ao criar pedido no Mercosistem";
      //estancia a classe com os parametros
      $error_handling = new error_handling($titulo, $nome_funcao, $saida, "erro");
      //estancia a função para criar a mensagem de corpo
      $error_handling->send_error_email();
      //estancia a função para executar as funções email()-db()-files() previamente
      //por padrão, as propriedades error_db e error_files estão true
      $error_handling->execute();
      echo "<br>Erro ao criar o pedido ";
      return false;
    } else {
      $merco_id = $this->mercosistem_order->get_order($order_id);
      $return =  $this->wooCommerceCreateOrderNote($order_id,"Id do Mercosistem: ".$merco_id->aCodigo);

      $corpo1 = "Pedido: ".$this->orderId.
      "<br>Pedido do WooCommerce: ". $result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aNumPedido.
      "<br>Pedido do Mercosistem: ".$merco_id->aCodigo;
      $corpo2 = "Cliente: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestRazao.
      "<br>E-mail: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestEmail.
      "<br>Data: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDataEmissao.
      "<br>Cep: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestCepEntr.
      "<br>Cidade: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestCidadeEntr.
      "<br>Estado: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestEstadoEntr.
      "<br>Endereço: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aDestEnderecoEntr.
      "<br>Subtotal da Compra: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aValorProdutos.
      "<br>Valor do Frete: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aValorFrete.
      "<br>Valor Total da Compra: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aCabecalho->aValorPedido.
      // "<br>PRODUTO: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aItem->aItemPedido->aCodigoProd.
      "<br>Qtde: ".$result->sBody->InserirPedidoResponse->InserirPedidoResult->aPedidoVenda->aItem->aItemPedido->aQtde;

      if(file_exists(str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.str_replace(' ','',$this->orderId).'.json')) {
        $mensagem_b2w = file_get_contents(str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.str_replace(' ','',$this->orderId).'.json');
        $corpo2 .= $mensagem_b2w;
      }
      $etiqueta = '';
      if(file_exists(str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.$this->orderId.'.pdf')) {
        $etiqueta = str_replace($_SERVER['SCRIPT_NAME'],'',$_SERVER['SCRIPT_FILENAME']).'/conectores/pedidos/'.$this->orderId.'.pdf';
      }
      //estancia a classe com os parametros
      $log = new log("Nova Compra MercoSistem", $corpo1, $corpo2, "nova compra");
      $log->dir_files = "log_files/log.json";
      $log->log_email = true;
      $log->log_etiqueta = $etiqueta;
      $log->log_db = true;
      $log->log_files = true;
      $log->mensagem_email = "Nova compra que entrou no mercosistem";
      $log->email_novacompra = true;
      //estancia a função para criar a mensagem de corpo
      $log->send_log_email();
      //estancia a função para executar as funções email()-db()-files() previamente
      //por padrão, as propriedades error_db e error_files estão true
      $log->execute();
      var_dump($result->sBody->InserirPedidoResponse->InserirPedidoResult);
      return "Pedido WooCommerce $order_id <b>OK</b>";
    }
  }

  public function mercoWcNormalizeOrderData($orderInformation)
  {
    $subtotal = 0;

    if(count($orderInformation->line_items) > 1) {
      foreach ($orderInformation->line_items as $key => $value) {
        $orderItems['ItemPedido'][] = [
            'CodigoProd' => $this->mercosistem_product->get_product_code($value->sku),
            'Desconto' => 0,
            'DescontoPercentual' => '0',
            'Qtde' => (int)$value->quantity,
            'ValUnit' => (float)$value->price
        ];
      $subtotal += $value->total;
    }
  } else {
    foreach ($orderInformation->line_items as $key => $value) {
      $orderItems['ItemPedido'] = [
          'CodigoProd' => $this->mercosistem_product->get_product_code($value->sku),
          'Desconto' => 0,
          'DescontoPercentual' => '0',
          'Qtde' => (int)$value->quantity,
          'ValUnit' => (float)$value->price
        ];
    $subtotal += $value->total;
  }
  }
  $shippingTotal = 0;
  foreach ($orderInformation->shipping_lines as $key => $value) {
    $shippingTotal += $value->total;
  }

  $destContato = $orderInformation->shipping->first_name." ".$orderInformation->shipping->last_name;
  if(strlen($destContato) > 29) {
    for ($i= 1; $i < strlen($destContato)-29; $i++) {
      $destContato = substr($destContato,0,strripos($destContato,' '));
    }
  }
    if(isset($this->metaDataWebSite['orderId'])) {
      $this->orderId = $this->metaDataWebSite['orderId'];
      $sReference = $this->metaDataWebSite['shippingReference'];
      $bReference = $this->metaDataWebSite['billingReference'];
      $discount = $this->metaDataWebSite['discount'] = $orderInformation->discount_total;
    } else {
      $this->orderId = $this->metaDataMktPlace['orderId'];
      $sReference = $this->metaDataMktPlace['shippingReference'];
      $bReference = $this->metaDataMktPlace['billingReference'];
      $discount = $this->metaDataMktPlace['discount'];
    }

    if($this->metaDataWebSite['_billing_cpf'] == '' || empty($this->metaDataWebSite['_billing_cpf']) || is_null($this->metaDataWebSite['_billing_cpf'])) $cpfCnpj = $this->customerData->cpf_cnpj;
    else $cpfCnpj = $this->metaDataWebSite['_billing_cpf'];


    $date = date('d-m-Y H:i:s', time());

    $shippName = $orderInformation->shipping->first_name." ".$orderInformation->shipping->last_name;


    $obs = "Pedido WooCommerce: ".$orderInformation->id.
    "\nPedido: ".$this->orderId.
    "\nCliente: ".$this->customerData->razao.
    "\nE-mail: ".$this->customerData->e_mail.
    "\nData: ".$orderInformation->date_created.
    "\nCEP: ".$this->customerData->cep_entr.
    "\nCidade: ".$this->customerData->cidade_entr.
    "\nEstado: ".$this->customerData->estado_entr.
    "\nEndereco: ".$this->customerData->end_entr.
    "\nReferencia do local de entrega: ".$sReference.
    "\nReferencia do local de cobrança: ".$bReference.
    "\nSubtotal da compra: ".$subtotal.
    "\nValor do frete: ".$shippingTotal.
    "\nValor Total da compra: ".$this->metaDataWebSite['totalToPay'].
    "\n";

    if(isset($this->metaDataMktPlace['commission'])) {
      $obs .= "Valor Comissão: ".$this->metaDataMktPlace['commission'];
    }
    if(isset($this->metaDataWebSite['shipping'])) {
      $obs .= "Valor Frete: ".$this->metaDataMktPlace['shipping'];
    }

    if(substr($this->customerData->fantasia,0,3) == 'MLB') $CodFormaPagto = 3;
    elseif(substr($this->customerData->fantasia,0,3) == 'B2W') $CodFormaPagto = 2;
    // elseif(substr($this->customerData->fantasia,0,3) == 'MGL') $site = 'magazineluiza.com.br';
    // elseif(substr($this->customerData->fantasia,0,3) == 'CNV') $site = 'lojista.cnova.com';
    else $CodFormaPagto = '';

      $this->orderData->CodCliOrig = $this->customerId;
      $this->orderData->CodEquipe = TEAM_CODE;
      $this->orderData->CodListaPreco = CODLISTAPRECO;
      $this->orderData->CodTransportadora = CODTRANSPORTADORA;//8117
      $this->orderData->CpfCnpj = $cpfCnpj;
      $this->orderData->DataEmissao = $date;
      $this->orderData->DataEntrega = $date;
      $this->orderData->DescontoPercentual = 0;
      $this->orderData->DestBairroEntr = $this->customerData->bairro_entr;
      $this->orderData->DestBairroPrinc = $this->customerData->bairro_entr;
      $this->orderData->DestCel = $this->customerData->fone_cel;
      $this->orderData->DestCepEntr = $this->customerData->cep_entr;
      $this->orderData->DestCepPrinc = $this->customerData->cep_entr;
      $this->orderData->DestCidadeEntr = $this->customerData->cidade_entr;
      $this->orderData->DestCidadePrinc = $this->customerData->cidade_entr;;
      $this->orderData->DestComplementoEntr = $this->customerData->compl_entr;
      $this->orderData->DestComplementoPrinc = $this->customerData->compl_entr;
      $this->orderData->DestContato = $destContato;
      $this->orderData->DestDataCadastro = $this->customerData->data_cadastro;
      $this->orderData->DestDataNasc = $this->customerData->data_nasc;
      $this->orderData->DestDddCel = $this->customerData->ddd_cel;
      $this->orderData->DestDddFone = $this->customerData->ddd_fixo;
      $this->orderData->DestEmail = $this->customerData->e_mail;
      $this->orderData->DestEnderecoEntr = $this->customerData->end_entr;
      $this->orderData->DestEnderecoPrinc = $this->customerData->end_entr;
      $this->orderData->DestEstadoEntr = $this->customerData->estado_entr;
      $this->orderData->DestEstadoPrinc = $this->customerData->estado_entr;
      // $this->orderData->DestFantasia;
      $this->orderData->DestFone = $this->customerData->fone_fixo;
      // $this->orderData->DestInscEstadual = '';
      // $this->orderData->DestInscMunicipal = '';
      $this->orderData->DestNumEndEntr = $this->customerData->num_end_entr;
      $this->orderData->DestNumEndPrinc = $this->customerData->num_end_entr;
      $this->orderData->DestRazao = $this->customerData->razao;
      $this->orderData->DestRg = '';
      $this->orderData->DestSexo = '';
      $this->orderData->DestTipoEndEntr = $this->customerData->tipo_end_entr;
      $this->orderData->DestTipoEndPrinc =$this->customerData->tipo_end_entr;
      $this->orderData->DestTipoPessoa = $this->customerData->tipo_pessoa;
      $this->orderData->NumPedido = $orderInformation->id;
      $this->orderData->NumSerie = NUMSERIE;
      $this->orderData->Obs = $obs;
      $this->orderData->Status = STATUS;
      if(isset($this->customerData->tipo_frete)) $this->orderData->TipoFrete = $this->customerData->tipo_frete;
      $this->orderData->TipoPedido = TIPOPEDIDO;
      $this->orderData->ValorDesconto = $discount;
      $this->orderData->ValorFrete = $shippingTotal;
      $this->orderData->ValorPedido = $orderInformation->total;
      $this->orderData->ValorProdutos = $subtotal;
      $this->orderData->ItemPedido = $orderItems;
      $this->orderData->CodCondicaoPagto = CODCONDICAOPAGTO;
      if(!empty($CodFormaPagto)) $this->orderData->CodFormaPagto = $CodFormaPagto;
      $this->orderData->Forma = $this->metaDataWebSite['paymentType'];
      $this->orderData->QtdeParcelas = $this->metaDataWebSite['parcels'];
      $this->orderData->Valor = $orderInformation->total;

      return $this->orderData;
  }

  public function mercoWcNormalizeCustomerData($orderInformation)
  {
    $metaDataOrderCustomer = [];
    foreach ($orderInformation->meta_data as $key => $value) {
      if($value->key == "birthday") $metaDataOrderCustomer['birthday'] = $value->value;
      if($value->key ==  "_shipping_neighborhood") $metaDataOrderCustomer['_shipping_neighborhood'] = $value->value;
      if($value->key ==  "_shipping_number") $metaDataOrderCustomer['_shipping_number'] = $value->value;
      if($value->key ==  "_billing_neighborhood") $metaDataOrderCustomer['_billing_neighborhood'] = $value->value;
      if($value->key ==  "_billing_number") $metaDataOrderCustomer['_billing_number'] = $value->value;
      if($value->key ==  "_billing_cpf") $metaDataOrderCustomer['_billing_cpf'] = $value->value;
    }

    if($orderInformation->customer_id != 0) {
      $customerInformations = $this->wooCommerceCustomer->wooCommerceGetCustomer($orderInformation->customer_id);
      // var_dump($customerInformations);
      // exit("wooCommerceGetCustomer()");
      if(isset($this->metaDataWebSite['buyer'])) $customer_info['name'] = $this->metaDataWebSite['buyer'];
      else $customer_info['name'] = $customerInformations->billing->first_name." ".$customerInformations->billing->last_name;
      $customer_info['city'] = $customerInformations->billing->city;
      $customer_info['state'] = $customerInformations->billing->state;
      $customer_info['postcode'] = preg_replace('/\D/', '',$customerInformations->billing->postcode);
      $customer_info['street'] = $orderInformation->billing->address_1;
      $customer_info['phone'] = preg_replace('/\D/', '',$customerInformations->billing->phone);

      if(isset($metaDataOrderCustomer['birthday'])) $customer_info['birthday'] = $metaDataOrderCustomer['birthday'];
      else $customer_info['birthday'] = '1979-01-01';

      foreach ($customerInformations->meta_data as $key => $value) {
        if($value->key == "CPF") $customerDocument = $value->value;
      }
      if(!isset($customerDocument)) $customerDocument = $orderInformation->billing->cpf;
      $customer_info['document'] = preg_replace('/\D/', '',$customerDocument);
      $customer_info['email'] = $customerInformations->billing->email;

      if(strlen($customer_info['state']) > 2) $customer_info['state'] = $this->mercosistem_customer->changestate($customer_info['state']);

      $customer_info['new_street'] = trim(explode(',',$customer_info['street'])[0]);
      $customer_info['number'] = trim(explode('-',explode(',',$customer_info['street'])[1])[0]);
      $customer_info['neighborhood'] = trim(explode('-',explode(',',$customer_info['street'])[1])[1]);

      $customer_info['complement'] = '';
      if($orderInformation->billing->address_2 != '') $customer_info['complement'] = $orderInformation->billing->address_2;
      elseif ($this->metaDataMktPlace['billingReference'] != '') $customer_info['complement'] = $this->metaDataMktPlace['billingReference'];
      $customer_info['sName'] = $customerInformations->shipping->first_name." ".$customerInformations->shipping->last_name;
      if($orderInformation->shipping->number == '') {
        $customer_info['sStreet'] = trim(explode(',',$orderInformation->shipping->address_1)[0]);
        $customer_info['sNumber'] = trim(explode('-',explode(',',$orderInformation->shipping->address_1)[1])[0]);
        $customer_info['sNeighborhood'] = trim(explode('-',explode(',',$orderInformation->shipping->address_1)[1])[1]);
      } else {
        $customer_info['sStreet'] = $orderInformation->shipping->address_1;
        $customer_info['sNumber'] = $orderInformation->shipping->number;
        $customer_info['sNeighborhood'] = $orderInformation->shipping->neighborhood;
      }
      $customer_info['sCity'] = $orderInformation->shipping->city;
      $customer_info['sState'] = $orderInformation->shipping->state;
      $customer_info['sPostcode'] = preg_replace('/\D/', '',$orderInformation->shipping->postcode);

      $customer_info['sComplement'] = '';
      if($orderInformation->shipping->address_2 != '') $customer_info['sComplement'] = $orderInformation->shipping->address_2;
      elseif ($this->metaDataMktPlace['shippingReference'] != '') $customer_info['sComplement'] = $this->metaDataMktPlace['shippingReference'];
    } else {
      if(isset($this->metaDataWebSite['buyer'])) $customer_info['name'] = $this->metaDataWebSite['buyer'];
      else $customer_info['name'] = $orderInformation->billing->first_name." ".$orderInformation->billing->last_name;
      $customer_info['email'] = $orderInformation->billing->email;
      if($orderInformation->billing->company != '') $customer_info['razao'] = $orderInformation->billing->company;
      if($orderInformation->billing->cnpj != '') $customer_info['document'] = $orderInformation->billing->cnpj;
      else $customer_info['document'] = $orderInformation->billing->cpf;
      $customer_info['city'] = $orderInformation->billing->city;
      $customer_info['state'] = $orderInformation->billing->state;
      $customer_info['postcode'] =  preg_replace('/\D/', '',$orderInformation->billing->postcode);
      $customer_info['phone'] = preg_replace('/\D/', '',$orderInformation->billing->phone);
      $customer_info['street'] = trim(preg_replace("/\([^)]+\)/","",$orderInformation->billing->address_1));
      $customer_info['new_street'] = substr($customer_info['street'], 0, strpos($customer_info['street'],','));
      $customer_info['number'] = $orderInformation->billing->number;
      $customer_info['neighborhood'] = $orderInformation->billing->neighborhood;
      $customer_info['birthday'] = '1979-01-01';
      if($orderInformation->billing->address_2 != '') $customer_info['complement'] = $orderInformation->billing->address_2;

      if(strlen($customer_info['state']) > 2) $customer_info['state'] = $this->mercosistem_customer->changestate($customer_info['state']);

      $customer_info['sName'] = $orderInformation->shipping->first_name." ".$orderInformation->shipping->last_name;
      $customer_info['sStreet'] = trim(preg_replace("/\([^)]+\)/","",$orderInformation->billing->address_1));
      $customer_info['sNumber'] = $orderInformation->shipping->number;
      $customer_info['sNeighborhood'] = $orderInformation->shipping->neighborhood;
      $customer_info['sCity'] = $orderInformation->shipping->city;
      $customer_info['sState'] = $orderInformation->shipping->state;
      $customer_info['sPostcode'] = preg_replace('/\D/', '',$orderInformation->shipping->postcode);

      if($orderInformation->shipping->address_2 != '') $customer_info['sComplement'] = $orderInformation->shipping->address_2;
    }

    $nomeContato = $customer_info['name'];
    if(strlen($nomeContato) > 29) {
      for ($i= 1; $i < strlen($nomeContato)-29; $i++) {
        $nomeContato = substr($nomeContato,0,strripos($nomeContato,' '));
      }
    }

    if(strlen($customer_info['document']) == 11) $tipo_pessoa = 0;
    if(strlen($customer_info['document']) == 14) $tipo_pessoa = 1;

    if(isset($customer_info['razao'])) $razao = $customer_info['razao'];
    else $razao = $customer_info['name'];

    if($customer_info['new_street'] == '' || is_null($customer_info['new_street'])) {
      $TipoEndereco = substr($this->mercosistem_order->normalize_data($customer_info['street']),0,strpos($customer_info['street'],' '));
      if(!is_bool(strpos($TipoEndereco,"EST")) || !is_bool(strpos($TipoEndereco,"AV"))) $TipoEndereco = 'RUA';
      $rua = trim(substr($this->mercosistem_order->normalize_data($customer_info['street']),strpos($customer_info['street'],' ')));
    } else {
      $TipoEndereco = substr($this->mercosistem_order->normalize_data($customer_info['new_street']),0,strpos($customer_info['new_street'],' '));
      if(!is_bool(strpos($TipoEndereco,"EST")) || !is_bool(strpos($TipoEndereco,"AV"))) $TipoEndereco = 'RUA';
      $rua = trim(substr($this->mercosistem_order->normalize_data($customer_info['new_street']),strpos($customer_info['new_street'],' ')));
    }

    $sTipoEndereco = substr($this->mercosistem_order->normalize_data($customer_info['sStreet']),0,strpos($customer_info['sStreet'],' '));
    if(!is_bool(strpos($sTipoEndereco,"EST")) || !is_bool(strpos($sTipoEndereco,"AV"))) $sTipoEndereco = 'RUA';
    $customer_info['sStreet'] = trim(substr($this->mercosistem_order->normalize_data($customer_info['sStreet']),strpos($customer_info['sStreet'],' ')));

    if(substr($customer_info['name'],0,3) == 'MLB') {
      $site = 'mercadolivre.com.br';
      $TipoFrete = '';
      $cod_forma_pagto = COD_FORMA_PAGTO_MLB;
    } elseif(substr($customer_info['name'],0,3) == 'B2W') {
      $site = 'b2wmarketplace.com.br';
      $TipoFrete = '';
      $cod_forma_pagto = COD_FORMA_PAGTO_B2W;
    } elseif(substr($customer_info['name'],0,3) == 'MGL') $site = 'magazineluiza.com.br';
    elseif(substr($customer_info['name'],0,3) == 'CNV') $site = 'lojista.cnova.com';
    else {
      $site = 'easypath.com.br';
      $TipoFrete = TIPOFRETE;
      $cod_forma_pagto = COD_FORMA_PAGTO_SITE;
    }

    $date = date('d-m-Y H:i:s', time());

    $ddd_telefone = '';
    $ddd_celular = '';
    $celular = '';
    $telefone = '';

    $numero = str_replace(' ','',(string)$customer_info['phone']);
    if(strlen($numero) == 10) {
      $ddd_telefone = substr($numero,0,2);
      $telefone = substr($numero,2);
    } else if(strlen($numero) == 11) {
      $ddd_celular = substr($numero,0,2);
      $celular = substr($numero,2);
    } else if(strlen($numero) == 9) {
      $celular = $numero;
    } else if(strlen($numero) == 8) {
      $telefone = $numero;
    }

    if($customer_info['state'] != "SP") $cfop = 108;
    else $cfop = 102;

    if(strlen($customer_info['complement']) > 30) $customer_info['complement'] = '';
    if(strlen($customer_info['sComplement']) > 30) $customer_info['sComplement'] = '';

    $this->customerData->tipo_pessoa = $tipo_pessoa;
    $this->customerData->razao = $razao;
    $this->customerData->fantasia = $customer_info['name'];
    $this->customerData->cpf_cnpj = $customer_info['document'];
    $this->customerData->rg = '';
    $this->customerData->tipo_endereco_princ = $TipoEndereco;
    $this->customerData->endereco_princ = $rua;
    $this->customerData->num_endereco_princ = $customer_info['number'];;
    $this->customerData->cep_endereco_princ = $customer_info['postcode'];
    $this->customerData->bairro_endereco_princ = $customer_info['neighborhood'];
    $this->customerData->cidade = $customer_info['city'];
    $this->customerData->estado = $customer_info['state'];
    $this->customerData->e_mail = $customer_info['email'];
    $this->customerData->nome_contato = $nomeContato;
    $this->customerData->site = $site;
    $this->customerData->tipo_end_cob = $TipoEndereco;
    $this->customerData->end_cob = $rua;
    $this->customerData->num_end_cob = $customer_info['number'];
    $this->customerData->cep_cob = $customer_info['postcode'];
    $this->customerData->bairro_cob = $customer_info['neighborhood'];
    $this->customerData->cidade_cob = $customer_info['city'];
    $this->customerData->estado_cob = $customer_info['state'];
    $this->customerData->tipo_end_entr = $sTipoEndereco;
    $this->customerData->end_entr = $customer_info['sStreet'];
    $this->customerData->num_end_entr = $customer_info['sNumber'];
    $this->customerData->cep_entr = $customer_info['sPostcode'];
    $this->customerData->bairro_entr = $customer_info['sNeighborhood'];
    $this->customerData->cidade_entr = $customer_info['sCity'];
    $this->customerData->estado_entr = $customer_info['sState'];
    $this->customerData->data_nasc = $customer_info['birthday'];
    // $this->customerData->insc_estadual = '';
    $this->customerData->sexo = '';
    // $this->customerData->estado_civil;
    // $this->customerData->insc_munincipal = '';
    $this->customerData->data_cadastro = $date;
    $this->customerData->codigo_cli_orig = '';
    $this->customerData->ddd_fixo = $ddd_telefone;
    $this->customerData->fone_fixo = $telefone;
    $this->customerData->ddd_cel = $ddd_celular;
    $this->customerData->fone_cel = $celular;
    $this->customerData->cfop_padrao = $cfop;
    $this->customerData->consumidor_final = CONSUMIDOR_FINAL;
    // $this->customerData->limite_credito;
    $this->customerData->cod_equipe = TEAM_CODE;
    $this->customerData->compl_end = $customer_info['complement'];
    $this->customerData->compl_cob = $customer_info['complement'];
    $this->customerData->compl_entr = $customer_info['sComplement'];

    if(!empty($TipoFrete)) $this->customerData->tipo_frete = $TipoFrete;
    $this->customerData->cod_lstpreco = COD_LSTPRECO;
    $this->customerData->cod_forma_pagto = $cod_forma_pagto;
    $this->customerData->prazo_medio_max_venda = PRAZO_MEDIO_MAX_VENDA;

    return $this->customerData;
  }

  public function wcmeliSameOrderPrice($orderId,$limit)
  {
    try {
      $orderInfo = $this->wooCommerceGetOrder($orderId);
      $orderPrice = $orderInfo->total;

      $salesOrder = $this->wooCommerceGetOrdersWithMetaData(['order'=>'desc']);

      if($limit < count($salesOrder)) {
        foreach ($salesOrder as $key => $value) {
          if($value->id != $orderId && $value->total == $orderPrice && $value->status == 'processing' || $value->status == 'complete') return true;
          if($limit-1 == $key) break;
        }
        return false;
      }

      foreach ($salesOrder as $key => $value) {
        if($value->id != $orderId && $value->total == $orderPrice && $value->status == 'processing' || $value->status == 'complete') return true;
      }
      return false;
    } catch(Exception $e) {
      $error = new error_handling("WCB2W: Erro ao buscar pedidos","Tentativa falha de buscar a lista de pedidos", "Erro<br>".$e->getMessage(), "Erro Pedido");
      $error->send_error_email();
      $error->execute();
      exit("Skyhub com problemas. Não foi possível retornar os pedidos");
    }
  }
}

?>
