<?php
Class mercowc_product extends wooCommerceProduct
{
  public function __construct()
  {
    parent::__construct();
    $this->product_id = '';
    $this->product_sku = '';
    $this->product_name = '';
    $this->product_price = '';
    $this->product_weight = '';
    $this->product_width = '';
    $this->product_length = '';
    $this->product_height = '';
    $this->product_description = '';
    $this->product_short_description = '';

    $this->mercosistem = new mercosistem_product;
  }

  public function update_products()
  {
    echo "<h2>Produto $this->product_id || $this->product_sku</h2>";
    $productMercoWc['id'] = (int)$this->product_id;
    if(TITLE) {
      if(!empty($this->product_name)) {
        if(strpos($this->product_name, "-")) {
          $productMercoWc['name'] = substr($this->product_name,0,strpos($this->product_name, "-"));
        } else $productMercoWc['name'] = $this->product_name;
      }
    } else echo "<h3>TITLE Desligado</h3>";

    if(DESCRIPTION) {
      if(!empty($this->product_description)) {
        $productMercoWc['description'] = $this->product_description;
        $productMercoWc['short_description'] = $this->product_short_description;
      }
    } else echo "<h3>DESCRIPTION Desligado</h3>";

    // if(!file_exists('atributosAusentes.json')) file_put_contents("atributosAusentes.json",'');
    // $WithoutAttributes = (array)json_decode(file_get_contents("atributosAusentes.json"));

    if(!empty($this->product_weight)) {
      $productMercoWc['weight'] = $this->product_weight;
    }
    // else {
    //   $WithoutAttributes[$this->product_id]->peso = $this->product_weight;
    //   file_put_contents("atributosAusentes.json",json_encode($WithoutAttributes));
    // }

    if(!empty($this->product_width)) {
      $productMercoWc['dimensions']['width'] = $this->product_width/100;
    }
    // else {
    //   $WithoutAttributes[$this->product_id]->largura = $this->product_width;
    //   file_put_contents("atributosAusentes.json",json_encode($WithoutAttributes));
    // }

    if(!empty($this->product_length)) {
      $productMercoWc['dimensions']['length'] = $this->product_length/100;
    }
    // else {
    //   $WithoutAttributes[$this->product_id]->comprimento = $this->product_length;
    //   file_put_contents("atributosAusentes.json",json_encode($WithoutAttributes));
    // }

    if(!empty($this->product_height)) {
      $productMercoWc['dimensions']['height'] = $this->product_height/100;
    }
    // else {
    //   $WithoutAttributes[$this->product_id]->altura = $this->product_height;
    //   file_put_contents("atributosAusentes.json",json_encode($WithoutAttributes));
    // }


    if(PRICE) {
      if(!empty($this->product_price)) {
        $productMercoWc['regular_price'] = $this->product_price;
      }
    }



    if(STOCK) {
      if($this->product_sku == "EP-51-60011") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35101");
      elseif ($this->product_sku == "EP-51-60021") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35011");
      elseif ($this->product_sku == "EP-51-60031") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35021");
      elseif ($this->product_sku == "EP-51-60041") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35041");
      elseif ($this->product_sku == "EP-51-60056") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35812");
      elseif ($this->product_sku == "EP-51-60061") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35051");
      elseif ($this->product_sku == "EP-51-60086") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35754");
      elseif ($this->product_sku == "EP-51-60116") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35755");
      elseif ($this->product_sku == "EP-51-60096") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35771");
      elseif ($this->product_sku == "EP-51-60106") $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty("EP-51-35772");

      else $productMercoWc['stock_quantity'] = $this->mercosistem->get_product_qty($this->product_sku);

      $productMercoWc['in_stock'] = true;
      if($productMercoWc['stock_quantity'] <= 0) $productMercoWc['in_stock'] = false;

    } else echo "<h3>STOCK Desligado</h3>";

    $productWoocommerce['update'] = [$productMercoWc];

    // var_dump($prodm);   //DEBUG
    if(!empty($productWoocommerce)) {
      $update_product = $this->wooCommerceProductBatch($productWoocommerce);

      if(!$update_product) {
        $error = new error_handling("MERCOSISTEM: Erro ao atualizar produto", "Erro ao tentar atualizar informações básicas do produto Id: $this->product_id || Sku: $this->product_sku", "Erro ao atualizar o produto", "Erro produto");
        $error->send_error_email();
        $error->execute();
        echo "Erro ao atualizar produto $this->product_id";
      } else echo "INFORMAÇÕES BÁSICAS: <b>OK</b><br>";
    } else throw new Exception('Informações sobre o produto estão vazias');
  }

  public function get_product_prices($sku,$price_list){
    // $sku = $this->get_product_code($sku);

    // $price_list = $this->get_prices();

    foreach ($price_list as $key => $value) {
      $descricao_list = $price_list[$key]->_descricao;
      if($descricao_list == "SITE") {
        $list = $price_list[$key]->_ListaProdutoList->ListaProduto;
        break;
      }
    }
    foreach ($list as $key => $value) {
      $product_id = (int)$list[$key]->_cod_prod;
      // $id = (int)$id;
      if($product_id == $sku) {
        $return = [];
        if(!empty($list[$key]->_vlr_1)) {
          $return['preco1'] = (float)$list[$key]->_vlr_1;
        } else {
          $return['preco1'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_2)) {
          $return['preco2'] = (float)$list[$key]->_vlr_2;
        } else {
          $return['preco2'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_3)) {
          $return['preco3'] = (float)$list[$key]->_vlr_3;
        } else {
          $return['preco3'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_4)) {
          $return['preco4'] = (float)$list[$key]->_vlr_4;
        } else {
          $return['preco4'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_5)) {
          $return['preco5'] = (float)$list[$key]->_vlr_5;
        } else {
          $return['preco5'] = (float)0;
        }

        if(!empty($list[$key]->_vlr_base)) {
          $return['valor_base'] = (float)$list[$key]->_vlr_base;
        } else {
          $return['valor_base'] = (float)0;
        }
        return $return;
        break;
      }
      $return['preco1'] = (float)0;
      $return['preco2'] = (float)0;
      $return['preco3'] = (float)0;
      $return['preco4'] = (float)0;
      $return['preco5'] = (float)0;
      $return['valor_base'] = (float)0;

      return $return;
    }
  }
}

?>
