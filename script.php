<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'include/all_include.php';
echo "<pre>";

// PRODUCTS
$time = time();
if(URL == 'http://erviegas.uk.to:4850/Service.svc?wsdl') echo "Utilizando wsdl de Homologação<br>";
  else {
    echo "<h1>PRODUTO</h1>";
    $mercowc_product = new mercowc_product;
    $flux_product = new flux('mercosistem_product');
    $flux_product->timeFile = true;
    $flux_product->pathListItem = true;
    $flux_product->nfeFile = false;
    $flux_product->storeOrderList = false;
    $flux_product->pagination = true;
    if(!$flux_product->setFiles()) {
      $listProductId = $mercowc_product->wooCommerceGetAllProductsIdSku();
      $flux_product->list_item = $listProductId;
      $flux_product->setFiles();
      exit('Lista de Produtos Criada');
    }
    $flux_product->getFiles();
    $flux_product->addCounter();
    $productSkuAndId = (array)$flux_product->test_next_item();

    if(strtolower(substr($productSkuAndId['sku'],0,3)) == 'grp') {
      $productId = $productSkuAndId['sku'];
      echo "Sku $productId dos pacotes. Não faz parte da atualização";
      var_dump($flux_product->add_item($productSkuAndId));
    } else {
      if(!$productSkuAndId) {
        echo "Todos os itens foram usados. Adicionando paginação na proxima execução";
        $flux_product->updatePaginationProduct(15);
      } else {
        $mercosistem_product = new mercosistem_product;
        $produt_info = $mercosistem_product->list_product($productSkuAndId['sku'])->sBody->ProdutoPorFiltroDisponiveisResponse->ProdutoPorFiltroDisponiveisResult->aProduto;
        $price_list = $mercosistem_product->get_product_prices($productSkuAndId['sku']);

        $mercowc_product->product_id = (int)$productSkuAndId['id']; //60
        $mercowc_product->product_sku = $productSkuAndId['sku']; //'EP-51-35215';
        $mercowc_product->product_name = (string)$produt_info->a_desc_reduz; //'ACQUALIFE SQUEEZE COM FILTRO 500ML | AMARELO';
        $mercowc_product->product_price = (float)$price_list['preco2']; //48.90;
        $mercowc_product->product_weight = (string)(string)$produt_info->a_peso_bruto; //0.08(kg);
        $mercowc_product->product_width = (string)$produt_info->a_largura; // 40(cm)
        $mercowc_product->product_length = (string)$produt_info->a_comprimento; // 40(cm);
        $mercowc_product->product_height = (string)$produt_info->a_altura; // 40(cm);
        $mercowc_product->product_description = nl2br($produt_info->a_aplicacao);
        $mercowc_product->product_short_description = nl2br($produt_info->a_obs);

        echo $mercowc_product->update_products();
      }

      $flux_product->add_item($productSkuAndId);
      echo "Tempo Produto: ";
      echo time() - $time;
      echo "<br>";
    }
  }

  // if(ORDER && $_SERVER['SERVER_NAME'] != 'localhost') {
    echo "<h1>PEDIDO</h1>";
    $mercowc_order = new mercowc_order;

    $flux_order = new flux('mercosistem_order');
    $flux_order->timeFile = true;
    $flux_order->nfeFile = false;
    $flux_order->storeOrderList = true;
    $flux_order->pathListItem = true;
    if(!$flux_order->setFiles()) {
      $listProductId = $mercowc_order->wooCommerceGetOrdersId();
      $flux_order->list_item = $listProductId;
      $flux_order->setFiles();
    }
    $flux_order->getFiles();
    $order_id = $flux_order->test_next_item();
    // var_dump($flux_order->list_item);       //DEBUG
    // exit;                      //DEBUG
    // $order_id = 2216;
    // var_dump($order_id);       //DEBUG
    // exit;                      //DEBUG
    if(!$order_id) echo "Sem novos pedidos<br>";
    else {
      echo "ID: ".$order_id."<br>";
      echo "Ja inserido? ";
      if($mercowc_order->mercosistem_order->get_order($order_id) !== false) echo "Já inserido!";
      $sameOrderPrice = $mercowc_order->wcmeliSameOrderPrice($order_id,10);
      var_dump($sameOrderPrice);
      if($sameOrderPrice) {
        $error = new error_handling("WCB2W: Pedidos com o mesmo preço","Foi confirmado a presença de pedidos com o mesmo preço", "Favor verificar e dar seguimento manualmente", "Erro Pedido");
        $error->send_error_email();
        $error->execute();
        exit("Pedido com o mesmo preço confirmado!!");
      }
      // exit;
      if(!$mercowc_order->mercosistem_order->get_order($order_id) && array_search($order_id,$flux_order->getOrderStoreList()) === false) {
        // exit("Algo errado");
        if($mercowc_order->mercoWcCreateOrder($order_id) != false) $flux_order->addOrderStore($order_id);
      } else {
        echo "<h3>Pedido $order_id já inserido no MercoSistem</h3>";
        $mercoOrder = $mercowc_order->mercosistem_order->get_order($order_id);
        if(!$mercoOrder) echo "<h4>Inserido Manuamente. Sem vínculo com o código do pedido no WooCommerce<h4>";
        else var_dump($mercoOrder);
      }
      // exit("Não vai gravar não");
      $flux_order->add_item($order_id);
    }
  // }
  if($_SERVER['SERVER_NAME'] == 'localhost') echo '<b>SCRIPT RODANDO EM LOCALHOST. ABORTADO BLOCO DE PEDIDO. <BR>RODAR SCRIPT EM easypath.com.br/conectores/mercosistem_v2/script.php</b>';
  echo "<br>";
  echo "Tempo Total: ";
  echo time() - $time;
  ?>
